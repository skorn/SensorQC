#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <string.h>
#include <stdlib.h>

#include <QTcpSocket>

#include "ProberBenchProber.h"

using namespace Suess;

ProberBenchProber::ProberBenchProber(std::string host, int port, 
    std::string name, bool notify, int client_id): 
  GenericProber()
{
  m_client_id = client_id;

  sock.reset(new QTcpSocket);
  sock->connectToHost(host.data(), port);
  if (!sock->waitForConnected(suess_tcp_timeout))
  {
    throw Exception( 
        "Suess::ProberBenchProber constructor: TCP Connection Timeout.");
  }
  
  if (sock->state() != QAbstractSocket::ConnectedState)
  {
    throw Exception("Suess::ProberBenchProber constructor: Not connected.");
  }
  send_name(name);
  send_notify(notify);
}

ProberBenchProber::~ProberBenchProber()
{
}

void ProberBenchProber::send_name(std::string name)
{
  std::stringstream ss;
  ss << "Name=" << name << p.eol;
  send(ss.str());
}
void ProberBenchProber::send_notify(bool notify)
{
  std::stringstream ss;
  ss << "Notify=" << notify << p.eol;
  send(ss.str());
}
      
void ProberBenchProber::send(std::string out)
{
  if (sock->state() != QAbstractSocket::ConnectedState)
  {
    
  }
  if (0) 
  {
    std::cerr << " >>PBP>> '" << out.substr(0, out.length() - p.eol.length()) << "'" << std::endl;
  }

  QByteArray block(out.data(), out.length());
  sock->write(block);
  sock->waitForBytesWritten(suess_tcp_timeout);
}
std::string ProberBenchProber::recv()
{
  if (sock->state() != QAbstractSocket::ConnectedState)
  {
    throw Exception("Suess::ProberBenchProber::recv: Not connected.");
  }
  QByteArray resp;
  int tries = 0;
  while((!resp.contains('\n')) && (tries++ < 600))
  {
    sock->waitForReadyRead(100);
    resp += sock->readAll();
    if (sock->state() != QAbstractSocket::ConnectedState)
    {
      throw Exception("Suess::ProberBenchProber::recv: Not connected.");
    }
  }
  
  int idx = resp.indexOf('\n');
  if (idx < 1)
    return std::string("");
  else
    return std::string(resp.constData(), idx - 1);
}
