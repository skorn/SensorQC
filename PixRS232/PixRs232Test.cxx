#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "PixRs232Device.h"
#ifdef WIN32
#include <windows.h>
#endif
#include <string.h>

using namespace std;


int main(int argc, char* argv[])
{
  std::string errtxt;

  if(argc!=2 && argc!=3){
    printf("USAGE: PixRs232Test [COM PORT NUMBER] <-u>\n");
    printf("    if -u is set (linux only): USB serial port device\n");
    return -1;
  }
  
  int tPortID = atoi(argv[1]);
  bool isUSB = false, runCmdl = false;
  if(argc==3 && strcmp(argv[2],"-u")==0) isUSB=true;
  if((argc==3 && strcmp(argv[2],"-c")==0) ||
     (argc==4 && strcmp(argv[3],"-c")==0)) runCmdl=true;
  
  if(isUSB)
    tPortID += PixRs232Device::USB1;
  else
    tPortID += PixRs232Device::COM1;
  
  PixRs232Device device((PixRs232Device::Portids)tPortID);
  
  if(device.getStatus()==PixRs232Device::COM_ERROR){
    device.getError(errtxt);
    printf("ERROR status: %s\n", errtxt.c_str());
  } else
    device.printDevice();

  if(runCmdl){
    std::cout << "Enter command or \"quit\" to terminate" << std::endl;
    std::string ucmd="";
    while(ucmd!="quit"){
      std::cin >> ucmd;
      if(ucmd!="quit"){
	std::string response;
	device.writeDevice(ucmd);
	device.readDevice(response);
	std::cout << "Reply: " << response << std::endl;
      }
    }
  }

  return 0;
}
