TEMPLATE = lib

unix {
  GCC_VERSION = $$system("g++ --version | grep g++")
  contains(GCC_VERSION, [5-9].[0-9].[0-9]) {
     CONFIG += c++14
  } else {
     contains(GCC_VERSION, 4.[8-9].[0-9]) {
       CONFIG += c++11
     } else {
       message( "unknown g++ version: ")
       message($$GCC_VERSION)
     }
  }
} else {
  CONFIG += c++11
}
CONFIG -= debug
CONFIG -= debug_and_release
CONFIG += release

SOURCES = simpati.cxx

DESTDIR = .
INCLUDEPATH += ../include

unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX
	QMAKE_LFLAGS += -lpthread
}

win32{
  DLLDESTDIR = ..\bin
  DESTDIR = ..\bin
  CONFIG += dll
  DEFINES += WIN32 PIX_DLL_EXPORT
  QMAKE_LFLAGS_RELEASE = delayimp.lib
  QMAKE_LFLAGS_WINDOWS += /LIBPATH:../bin
}

QT = network core
