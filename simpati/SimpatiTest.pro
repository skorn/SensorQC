CONFIG -= qt

TEMPLATE = app

unix {
  GCC_VERSION = $$system("g++ --version | grep g++")
  contains(GCC_VERSION, [5-9].[0-9].[0-9]) {
     CONFIG += c++14
  } else {
     contains(GCC_VERSION, 4.[8-9].[0-9]) {
       CONFIG += c++11
     } else {
       message( "unknown g++ version: ")
       message($$GCC_VERSION)
     }
  }
} else {
  CONFIG += c++11
}
CONFIG -= debug
CONFIG -= debug_and_release
CONFIG += release

SOURCES += SimpatiTest.cxx

INCLUDEPATH += ../include
unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX
	QMAKE_LFLAGS += -L . -lpthread -lsimpati
	QMAKE_RPATHDIR += ../simpati
}
win32 {
    DESTDIR = ../bin
    CONFIG += console
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
    DEFINES += "_CRT_SECURE_NO_WARNINGS" 
    DEFINES += __VISUALC__ 
    QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
    QMAKE_LFLAGS_RELEASE = delayimp.lib simpati.lib
    QMAKE_LFLAGS_CONSOLE += /LIBPATH:../bin
}

QT += network core
