#include <TGraph.h> 
#include <TF1.h>
#include <TApplication.h>
#include <TAxis.h>
#include <TMath.h>
#include <TSystem.h>
#include <TCanvas.h>

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <string.h>
#include <time.h>
#include "upsleep.h"

#include "PixGPIBDevice.h"

int main(int argc, char **argv){
  int board=0, PAD=20, SAD=0, mySleep=2, nCycl = 5;

  for(int i=0;i<argc;i++){
    if(i<(argc-1) && strcmp(argv[i],"-p")==0)
      PAD = atoi(argv[i+1]);
    if(i<(argc-1) && strcmp(argv[i],"-w")==0)
      mySleep = atoi(argv[i+1]);
    if(i<(argc-1) && strcmp(argv[i],"-c")==0)
      nCycl = atoi(argv[i+1]);
  }
    
  TApplication app("app",NULL,NULL);

  TGraph gc;

  const int nCal=130;
  float temp_nom[nCal]{-60, -59, -58, -57, -56, -55, -54, -53, -52, -51, -50, -49, -48, -47, -46, -45, -44, -43, -42, -41, 
                       -40, -39, -38, -37, -36, -35, -34, -33, -32, -31, -30, -29, -28, -27, -26, -25, -24, -23, -22, -21, 
                       -20, -19, -18, -17, -16, -15, -14, -13, -12, -11, -10,  -9,  -8,  -7,  -6,  -5,  -4,  -3,  -2,  -1,   
                         0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19, 
                        20,  21,  22,  23,  24,  25,  26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  
                        40,  41,  42,  43,  44,  45,  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  
                        60,  61,  62,  63,  64,  65,  66,  67,  68,  69};

  float res_nom[nCal] = {207.1, 192.5, 179.0, 166.6, 155.1, 144.6, 134.8, 125.8, 117.4, 109.7, 102.6, 95.92, 89.77, 84.07, 78.77, 73.85, 69.28, 65.02, 61.06, 57.37, 
			 53.94, 50.70, 47.67, 44.86, 42.23, 39.77, 37.48, 35.33, 33.33, 31.45, 29.69, 28.04, 26.49, 25.03, 23.67, 22.39, 21.19, 20.06, 19.00, 18.00, 
			 17.07, 16.18, 15.34, 14.55, 13.80, 13.10, 12.44, 11.82, 11.24, 10.68, 10.16, 9.664, 9.196, 8.753, 8.335, 7.940, 7.566, 7.212, 6.878, 6.561, 
			 6.261, 5.966, 5.686, 5.422, 5.172, 4.935, 4.710, 4.497, 4.295, 4.104, 3.922, 3.743, 3.573, 3.411, 3.258, 3.113, 2.976, 2.845, 2.721, 2.603, 
			 2.491, 2.382, 2.279, 2.182, 2.088, 2.000, 1.915, 1.834, 1.758, 1.685, 1.615, 1.548, 1.484, 1.423, 1.365, 1.310, 1.257, 1.207, 1.159, 1.113, 
			 1.070, 1.028,0.9874,0.9490,0.9123,0.8772,0.8437,0.8116,0.7810,0.7517,0.7237,0.6966,0.6707,0.6459,0.6222,0.5995,0.5777,0.5569,0.5369,0.5178, 
			 0.4994, 0.4817, 0.4647, 0.4484, 0.4328, 0.4178, 0.4034, 0.3896, 0.3763, 0.3635};



  for(int i=0;i<nCal;i++)
    gc.SetPoint(i, 1.e3*res_nom[i], temp_nom[i]);
  gc.SetMarkerStyle(4);
  TF1 f("ntccal","1/([0]+[1]*TMath::Log(x)+[2]*TMath::Power(TMath::Log(x),3))+[3]", -100, 100);
  f.SetParameter(0,2.e-3);
  f.SetParameter(1,3.e-4);
  f.SetParameter(2,1.e-6);
  f.SetParameter(3,175);
  gc.Fit("ntccal","0q");
  float cal[4];
  for(int i=0;i<4;i++) cal[i] = f.GetParameter(i);
  std::cout << "cal pars: " << cal[0] << ", " << 
    cal[1] << ", " <<
    cal[2] << ", " <<
    cal[3] << ", " <<std::endl;

  TCanvas can("mycan","Temperature",600,600);
  can.Clear();
  can.cd();
  gSystem->ProcessEvents();

  TGraph gt;
  gt.SetMarkerStyle(20);
  time_t  timev, toffset=0;
  float res, temp;

  PixGPIBDevice *m_dev = new PixGPIBDevice(board,PAD,SAD,0);

  for(int i=0;i<nCycl;i++){

    m_dev->measureResistances();
    res = m_dev->getResistance(0);

    time(&timev);
    if(i==0) toffset = timev;
    timev -= toffset;

    temp = 1/(cal[0]+cal[1]*TMath::Log(res)+cal[2]*TMath::Power(TMath::Log(res),3))+cal[3];
    std::cout << "T = " << temp << " degC" << " at " << timev << "s" << std::endl;

    gt.SetPoint(gt.GetN(), timev, temp);
    if(i>1){
      gt.Draw("AP");
      gt.GetXaxis()->SetTimeDisplay(1);
      gt.GetXaxis()->SetTimeFormat("%H:%M:%S");
      gt.GetXaxis()->SetTitle("Time [h:m:s]");
      gt.GetYaxis()->SetTitle("Temperature [#circ C]");
      gt.GetYaxis()->SetTitleOffset(1.2);
      can.Update();
      gSystem->ProcessEvents();
    }

    if(i<(nCycl-1)) UPGen::Sleep(1000*mySleep);

  }

  delete m_dev;


  std::cout << "Press ctrl-C to stop" << std::endl;
  app.Run();

  
  return 0;
}

