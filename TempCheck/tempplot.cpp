void tempplot(float nomT=20., char *fname_in=0){
  char fname[150];
  sprintf(fname,"/home/jgrosse/Pixel/data_set%d.txt",(int)(10*nomT+0.5));
  if(fname_in!=0) sprintf(fname,"%s",fname_in);
  FILE *in = fopen(fname,"r");
  if(in==0){
    printf("Can't open file %s\n", fname);
    return;
  }

  TCanvas *can = (TCanvas*) gROOT->FindObject("Tcan");
  if(can==0) can = new TCanvas("Tcan","Temperature",600,600);
  else can->Clear();
  can->cd();
  gSystem->ProcessEvents();

  TGraphErrors *gt = new TGraphErrors();
  gt->SetMarkerStyle(20);

  float temp, time, time0=-1;

  while(fscanf(in, "T = %f degC at %fs\n", &temp, &time)==2){
    if(time0<0) time0=time;
    time -= time0;
    gt->SetPoint(gt->GetN(), time, temp);
    gt->SetPointError(gt->GetN()-1, 0., 0.005*temp);
  }

  fclose(in);

  gt->Draw("AP");
  gt->GetXaxis()->SetTimeDisplay(1);
  gt->GetXaxis()->SetTimeFormat("%M");
  gt->GetXaxis()->SetTitle("Time [min.]");
  gt->GetYaxis()->SetRangeUser(nomT*.9,nomT*1.1);
  gt->GetYaxis()->SetTitle("Temperature [#circ C]");
  gt->GetYaxis()->SetTitleOffset(1.4);

  TLine *l = new TLine(0, nomT, time, nomT);
  l->SetLineColor(2);
  l->SetLineStyle(2);
  l->Draw();

  TLegend *leg = new TLegend(.6, .8, .95, .95);
  leg->AddEntry(gt, "Measurement", "p");
  leg->AddEntry(l, "Controller Setting", "l");
  leg->Draw();

  can->Update();
}
