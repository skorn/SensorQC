/* ******************************
   * CVScan 1.0                 *
   * jgrosse1@uni-goettingen.de *
   ****************************** */

#include <TGraphErrors.h> 
#include <TApplication.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <iostream>
#include <vector>
#include <fstream>
#include <ctime>
#include <chrono>
#include "upsleep.h"
#include <math.h>
#include <iomanip>
#include <thread>
#include <cmath>

#include "PixGPIBDevice.h"
#include "PixRs232Device.h"

void procInput(TApplication *app){
    std::string response="";
    std::cout << "Modify Canvas as needed, then enter \"quit\" to quit." << std::endl;
    while(response!="quit"){
      std::cin >> response;
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    std::cout << "Preparing plots for writing to file, please wait..." << std::endl;
    app->Terminate(); 
}

void measCap(PixRs232Device &lcr, int nr_meas, double cfreq, double &avg_c, double &sigma_c){
  double sum_c=0., sum_c_sqr=0., temp_c;
  for(int n=0; n<nr_meas; n++){		// measure n times and average
    temp_c = lcr.getCapacity(0, cfreq);	//actual measurement
    sum_c += temp_c;
    sum_c_sqr += temp_c*temp_c;
    UPGen::Sleep(100);			//wait for 100ms before the next measurement
  }
  avg_c = sum_c/(double)nr_meas;				//calculate average
  sigma_c = sum_c_sqr/(double)nr_meas;				//calculate sq. average
  sigma_c = sqrt((sigma_c-avg_c*avg_c)/(double)(nr_meas-1));	//calculate std. dev.
}

int main(int argc, const char* argv[]){
	
  int PID=24, PID_T=-1, myCOM=1;
  double step_volt = 5;		//step size for voltage increase
  int nr_meas = 4;		//no. of measurements at each voltage
  double max_volt = -250;	//maximum voltage during scan	
  double cfreq = 1.e4;

  if(argc<4){
    std::cerr << "No sensor/tile ID or tester name provided, won't proceed! " << std::endl;
    std::cout << "Usage:\nCVscan [-p GPIB-ID of HV device (-1: not used)] [-t GPIB-ID of temp. sensor meter (-1: not used)] "
	      << "[-c COM-port-ID of LCR meter] [-s voltage step size] [-v max. voltage] [-n no. of measurements per voltage step] "
	      << "[-f frequence of C-measurement]" << std::endl;
    return 1;
  }

  for (int i=1;i<argc;i++){
    if(std::string(argv[i])=="-h"){
      std::cout << "Usage:\nCVscan [-p GPIB-ID of HV device (-1: not used)] [-t GPIB-ID of temp. sensor meter (-1: not used)] "
		<< "[-c COM-port-ID of LCR meter] [-s voltage step size] [-v max. voltage] [-n no. of measurements per voltage step] "
		<< "[-f frequence of C-measurement]" << std::endl;
      return -2;
    }
    if(std::string(argv[i])=="-p" && i<(argc-1)){
      PID=atoi(argv[i+1]);
      i++;
    }
    if(std::string(argv[i])=="-t" && i<(argc-1)){
      PID_T=atoi(argv[i+1]);
      i++;
    }
    if(std::string(argv[i])=="-c" && i<(argc-1)){
      myCOM=atoi(argv[i+1]);
      i++;
    }
    if(std::string(argv[i])=="-s" && i<(argc-1)){
      step_volt=(double)atof(argv[i+1]);
      i++;
    }
    if(std::string(argv[i])=="-n" && i<(argc-1)){
      nr_meas=atoi(argv[i+1]);
      i++;
    }
    if(std::string(argv[i])=="-v" && i<(argc-1)){
      max_volt=(double)atof(argv[i+1]);
      i++;
    }
    if(std::string(argv[i])=="-f" && i<(argc-1)){
      cfreq=(double)atof(argv[i+1]);
      i++;
    }
  }

	
  double avg_c, sigma_c;
  std::vector<double> volt;
  
  // don't start from exactly 0V
  double vi=std::copysign(1.0, max_volt);
  volt.push_back(vi);  
  vi = 0.;
  // flexibly set point depending on sign of max_volt
  if(max_volt<0.){
    while(vi > max_volt){
      vi-=step_volt;
      volt.push_back(vi);
    }
  } else {
    while(vi < max_volt){
      vi+=step_volt;
      volt.push_back(vi);
    }
  }

  //create output file name and open file
  std::ofstream outfile;
  std::stringstream fnamestream;
  std::string fname;
  
  fnamestream<<argv[1]<<"_"<<argv[2]<<"_CV.raw";
  fname = fnamestream.str();
  outfile.open(fname.c_str(), std::ofstream::out | std::ofstream::trunc);
  
  //get date and time
  std::chrono::time_point<std::chrono::system_clock> start;
  start = std::chrono::system_clock::now();
  std::time_t scan_time = std::chrono::system_clock::to_time_t(start);
  
  outfile << "Wafer " << argv[1] << std::endl;
  outfile << "Tile no. " << argv[2] << std::endl;
  outfile << "Scan done " << std::ctime(&scan_time); //std::ctime() seems to put std::endl
  outfile << "by "<<argv[3]<<" at University of Goetingen"<<std::endl;
  outfile << std::endl;
  outfile << "voltage [V] \t avg. current [A] \t avg. cap. [F] \t std. dev. [F]" << std::endl;
	
  // create devices for HV SMU, temp, meter, LCR meter
  PixGPIBDevice *myDevice = 0;
  if(PID>=0) {
    myDevice = new PixGPIBDevice(0, PID, 1, 0);
    if(myDevice->getStatus()==PixGPIBDevice::PGD_ERROR){
      delete myDevice; myDevice=0;
      std::cerr << "Can't find HV for bias, will continue with simple C test" << std::endl;
    } else{
      std::cout << "HV Device is " << myDevice->getDescription() << std::endl;
      std::cout<<"HV Device has "<<myDevice->getDeviceNumberChannels()<<" channels"<<std::endl;
      
      if(myDevice->getDeviceFunction()!=SUPPLY_HV){
	std::cout <<"Not an HV power supply. IV scan not possible, will continue with simple C test." << std::endl;
	delete myDevice; myDevice=0;
      }
    }
  } else
    std::cerr << "No PID for HV for bias provided, will continue with simple C test" << std::endl;

  PixGPIBDevice *tdev = 0;
  if(PID_T>=0){
    tdev = new PixGPIBDevice(0, PID_T, 1, 0);
    std::cout << "NTC Device is " << tdev->getDescription() << std::endl;
    std::cout << "NTC Device has "<<tdev->getDeviceNumberChannels()<<" channels"<<std::endl;
  }

  if(myDevice!=0){
    // initialize power supply
    if(myDevice->getStatus() == PixGPIBDevice::PGD_ON){	//if the PS is on, turn it off
      myDevice->setState(PixGPIBDevice::PGD_OFF);
      UPGen::Sleep(100);
    }
    
    myDevice->setCurrentLimit(0, 100E-6);
    myDevice->setState(PixGPIBDevice::PGD_ON);
    myDevice->measureCurrents();
    UPGen::Sleep(100);
  }

  PixRs232Device lcr((PixRs232Device::Portids)(myCOM-1+PixRs232Device::COM1));
  std::cout << "LCR Device is " << lcr.getDescription() << std::endl;
  
  int m_argc=argc;
  char **m_argv = new char*[argc];
  for(int i=0;i<argc;i++){
    m_argv[i] = new char[100];
    sprintf(m_argv[i], "%s",argv[i]);
  }
  TApplication *app = new TApplication("pixel data viewer", &m_argc, m_argv);
  // preventing problems with LLVM/OpenGL, see 
  // https://root-forum.cern.ch/t/error-llvm-symbols-exposed-to-cling/23597/2
  gROOT->GetInterpreter();
  
  TCanvas can("mycan", "sensor characterisation", 600, 600);
  can.Clear();
  can.cd();
  
  TGraphErrors* m_graph = new TGraphErrors();	//create empty graph
  float cal[4] = {0.0020091, 0.000296397, 1.65669e-06, -175.422}; // calib, of NTC

  /* ***** start of measurement loop ***** */
  if(myDevice!=0){
    for(unsigned int j=0; j<volt.size(); j++){
      if(tdev!=0){
	tdev->measureResistances();
	float res = tdev->getResistance(0);
	float temp = 1/(cal[0]+cal[1]*TMath::Log(res)+cal[2]*TMath::Power(TMath::Log(res),3))+cal[3];
	std::cout << ".. T = " << std::setprecision(3) << temp << "°C" << std::endl;
      }
    
      std::cout<<"INFO: step no. "<<j<<":\t voltage: "<<volt[j]<<" V";
      myDevice->setVoltage(0, volt[j]);
      // wait to make sure DUT stabilises
      UPGen::Sleep(1000);

      // current measurement to allow early abort
      myDevice->measureCurrents();
      double temp_i = myDevice->getCurrent(0);
      std::cout<<"\t current: "<<temp_i<<" A";
      // cap. meas.
      measCap(lcr, nr_meas, cfreq, avg_c, sigma_c);
      outfile << volt[j] <<"\t"<<std::scientific<< temp_i<<"\t"<< avg_c <<"\t"<< sigma_c<<std::endl;
      std::cout<<"\t capacitance: "<<avg_c<<" F";
      
      m_graph->SetPoint(j, volt[j], avg_c*1.e12);
      m_graph->SetPointError(j,0,sigma_c*1.e12);
      
      char gtit[200];
      sprintf(gtit, "CV curve");// wafer %s tile %s", argv[1], argv[2]);
      m_graph->SetTitle(gtit);
      m_graph->SetMarkerStyle(20);
      m_graph->SetMarkerSize(0.5);
      m_graph->GetXaxis()->SetTitle("bias voltage [V]");
      sprintf(gtit, "C(f=%.0f kHz) [pF]", cfreq/1.e3);
      m_graph->GetYaxis()->SetTitle(gtit);
      m_graph->Draw("AP");
      can.Update();
	  gSystem->ProcessEvents();
      if(std::abs(temp_i)>=50E-6){
	std::cout<<"\t software current limit reached"<<std::endl; 
	break;
      } else std::cout<<std::endl;
    }
  
    /* ***** end of measurement loop ***** */
  
    /* ***** cleaning up ***** */
  
    delete tdev;
    myDevice->setVoltage(0, 0.0);
    outfile.close();
	
    if(myDevice->getStatus()==PixGPIBDevice::PGD_ON) {
      std::cout << "Found status PGD_ON. Turning off" << std::endl;
      myDevice->setState(PixGPIBDevice::PGD_OFF);
      UPGen::Sleep(100);		//for some reason have to wait after changing status
    }
    delete myDevice;

  } else {

    int lcr_frq[17]={10,12,15,18,20,24,25,30,36,40,45,50,60,72,75,80,90};
    std::string cmdstrg;
    for(int i=0; i<34;i++){
      double freq = (double)((i<17)?(lcr_frq[i]*1e2):(lcr_frq[i-17]*1e3));
      
      measCap(lcr, nr_meas, freq, avg_c, sigma_c);
      m_graph->SetPoint(i, freq/1.e3, avg_c*1.e12);
      m_graph->SetPointError(i,0.,sigma_c*1.e12);
      
      char gtit[200];
      sprintf(gtit, "CV curve");// wafer %s tile %s", argv[1], argv[2]);
      m_graph->SetTitle(gtit);
      m_graph->SetMarkerStyle(20);
      m_graph->SetMarkerSize(0.5);
      m_graph->GetXaxis()->SetTitle("f [kHz]");//bias voltage [V]");
      m_graph->GetYaxis()->SetTitle("C [pF]");
      m_graph->Draw("AP");
      can.Update();
      
    }
    
    if(lcr.getStatus() != PixRs232Device::COM_OK){
      lcr.getError(cmdstrg);
      std::cerr << "Error in meas. cycle: " << cmdstrg << std::endl;
    }
  }
    
  std::thread thr(procInput, app);
  app->Run(true);
  app->SetReturnFromRun(false);
  
  // will continue once separate thread stops TApplication
  fname.erase(fname.find_last_of("."), 4);
  can.SaveAs(fname.append(".root").c_str());
  fname.erase(fname.find_last_of("."), 5);
  can.SaveAs(fname.append(".pdf").c_str());

  app->Terminate();
  delete m_graph;
  delete app;

}
