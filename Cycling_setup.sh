#!/bin/sh

echo "************************************************************************"
echo -e "SETUP\t This is the Cycling setup script"
echo -e "SETUP\t "
echo -e "SETUP\t For more info, consult the README pages"
echo ""


# Determine where this script is located (this is surprisingly difficult).
if [ "${BASH_SOURCE[0]}" != "" ]; then
    # This should work in bash.
    _src=${BASH_SOURCE[0]}
elif [ "${ZSH_NAME}" != "" ]; then
    # And this in zsh.
    _src=${(%):-%x}
elif [ "${1}" != "" ]; then
    # If none of the above works, we take it from the command line.
    _src="${1/setup.sh/}/setup.sh"
else
    echo -e "SETUP\t Failed to determine the Cycling base directory. Aborting ..."
    echo -e "SETUP\t Can you give the source script location as additional argument? E.g. with"
    echo -e "SETUP\t . ../foo/bar/scripts/setup.sh ../foo/bar/scripts"
    return 1
fi

export Cycling_BASE_DIR="$(cd -P "$(dirname "${_src}")" && pwd)"

echo "************************************************************************"
echo -e "SETUP\t Configuration finished!"
