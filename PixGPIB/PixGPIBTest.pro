CONFIG -= qt

TEMPLATE = app

unix {
  GCC_VERSION = $$system("g++ --version | grep g++")
  contains(GCC_VERSION, [5-9].[0-9].[0-9]) {
     CONFIG += c++14
  } else {
     contains(GCC_VERSION, 4.[8-9].[0-9]) {
       CONFIG += c++11
     } else {
       message( "unknown g++ version: ")
       message($$GCC_VERSION)
     }
  }
} else {
  CONFIG += c++11
}
CONFIG -= debug
CONFIG -= debug_and_release
CONFIG += release

SOURCES += PixGPIBTest.cxx
SOURCES += PixGPIB.cxx
SOURCES += PixGPIBDevice.cxx
SOURCES += PixGPIBError.cxx

INCLUDEPATH = . ../include

unix {
    DESTDIR = .
    QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
    QMAKE_LFLAGS += -lgpib -lpthread
}
win32 {
    CONFIG += console
    DESTDIR = ../bin
    LIBS += Gpib-32.obj
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
}
