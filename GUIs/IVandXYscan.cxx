#include <TCanvas.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TFile.h>
#include <TROOT.h>

#include "QRootApplication.h"
#include "IVandXYscan.h"
#include "PixGPIBDevice.h"

#include <iostream>
#include <sstream>
#include <exception>
#include <string>
#include <ctime>
#include <chrono>
#include <fstream>

#include <QString>
#include <QTimer>
#include <QApplication>
#include <QFileDialog>
#include <QDockWidget>

#include "GenericProber.h"
#include "VeloxRS232Prober.h"
#include "ProberBenchProber.h"

#include "IVscan.h"
#include "ProberControl.h"
#include "TempMon.h"

IVandXYscan::IVandXYscan(QWidget * parent, Qt::WindowFlags flags) : QMainWindow(parent, flags) {

  setWindowTitle("IV and XY scan panel");

  QDockWidget* dockWidget_1 = new QDockWidget(this);
  dockWidget_1->setObjectName(QStringLiteral("dockWidget_1"));
  dockWidget_1->setWindowTitle("IV Control");
  QDockWidget* dockWidget_2 = new QDockWidget(this);
  dockWidget_2->setObjectName(QStringLiteral("dockWidget_2"));
  dockWidget_2->setWindowTitle("Prober Control");
  QDockWidget* dockWidget_3 = new QDockWidget(this);
  dockWidget_3->setObjectName(QStringLiteral("dockWidget_3"));
  dockWidget_3->setWindowTitle("Environment Monitoring");

  flagAbortScan=false;
  
  ivscan = new IVscan(this, Qt::Window);
  probercontrol = new ProberControl(this, Qt::Window, true);
  tempmon = new TempMon(this, Qt::Window);

  dockWidget_1->setWidget(ivscan);
  dockWidget_2->setWidget(probercontrol);
  dockWidget_3->setWidget(tempmon);

  addDockWidget(Qt::LeftDockWidgetArea,  dockWidget_1);
  addDockWidget(Qt::RightDockWidgetArea, dockWidget_2);
  addDockWidget(Qt::RightDockWidgetArea, dockWidget_3);

// disconnect(ivscan->startButton, SIGNAL(clicked()), ivscan, SLOT(startScan()));
//  connect(ivscan->startButton, SIGNAL(clicked()), this, SLOT(startIVandXYscan()));
//  ivscan->startButton->setText("Start IV-XY Scan");
  connect(probercontrol->checkBoxIVafterStep, SIGNAL(clicked()), this, SLOT(enableIVandXYscan()));
  connect(tempmon, SIGNAL(newTempDev(QList<QString>)), ivscan, SLOT(fillTemperBox(QList<QString>)));
  connect(tempmon, SIGNAL(obsTempDev(QList<QString>)), ivscan, SLOT(clearTemperBox(QList<QString>)));
  connect(tempmon, SIGNAL(newTempVal(QMap<QString, double>)), ivscan, SLOT(fillTempVal(QMap<QString, double>)));
}

IVandXYscan::~IVandXYscan(){
//   delete ivscan;
//   delete probercontrol;
 
}

void IVandXYscan::startIVandXYscan(){
  std::chrono::time_point<std::chrono::system_clock> startTimeIVandXYscan = std::chrono::system_clock::now();
  std::time_t startTimeIVandXYscan2 = std::chrono::system_clock::to_time_t(startTimeIVandXYscan);
  std::cout << std::endl << "Start XY and IV scan at " << std::ctime(&startTimeIVandXYscan2) << std::endl;
  probercontrol->moveXYbutton->setEnabled(false);
  probercontrol->moveZbutton->setEnabled(false);
  //probercontrol->scanXYbutton->setEnabled(false);
  probercontrol->connectTimerToReadProberPosition(false);
  disconnect(probercontrol->scanXYbutton, SIGNAL(clicked()), this, SLOT(startIVandXYscan()));
  connect(probercontrol->scanXYbutton, SIGNAL(clicked()), this, SLOT(flagAbortScanToTrue()));
  probercontrol->scanXYbutton->setText("Abort XY+IV Scan");
  ivscan->startButton->setEnabled(false);
  // read current position
  Suess::ReadChuckPositionResponse pos(probercontrol->m_prober->ReadChuckPosition('Y', 'H', 'D'));
  double x0 = pos.r_x, y0 = pos.r_y;
  double xStepSize = probercontrol->spinBoxXstepSize->value();
  double yStepSize = probercontrol->spinBoxYstepSize->value();
  // start scan
  ivscan->iScan=0;
  ivscan->NScan=probercontrol->spinBoxXsteps->value() * probercontrol->spinBoxYsteps->value();
  for(int iY=0; iY<probercontrol->spinBoxYsteps->value(); iY++){
    if(flagAbortScan) break;
    for(int iX=0; iX<probercontrol->spinBoxXsteps->value(); iX++){
      if(flagAbortScan) break;
      probercontrol->m_prober->MoveChuckWait(x0 + iX*xStepSize, y0 + iY*yStepSize);
      probercontrol->readDev();
      probercontrol->stepX->display(iX+1); probercontrol->stepY->display(iY+1);
      probercontrol->relX->display(iX*xStepSize); probercontrol->relY->display(iY*yStepSize);
      std::this_thread::sleep_for(std::chrono::milliseconds((int)probercontrol->spinBoxWaitTime->value()));
      ivscan->stepX = iX+1; ivscan->stepY = iY+1;
      ivscan->posX = iX*xStepSize; ivscan->posY = iY*yStepSize;
      ivscan->startScan(); // do IV scan here
      ivscan->iScan++;
    }
  }
  ivscan->iScan=0;
  ivscan->NScan=0;
  //ivscan->deleteIVgraphs();
  // move back to original position
  probercontrol->m_prober->MoveChuckWait(x0, y0);
  probercontrol->stepX->display(0); probercontrol->stepY->display(0);
  probercontrol->relX->display(0); probercontrol->relY->display(0);
  // enable again buttons
  probercontrol->moveXYbutton->setEnabled(true);
  probercontrol->moveZbutton->setEnabled(true);
  //probercontrol->scanXYbutton->setEnabled(true);
  probercontrol->connectTimerToReadProberPosition(true);
  disconnect(probercontrol->scanXYbutton, SIGNAL(clicked()), this, SLOT(flagAbortScanToTrue()));
  connect(probercontrol->scanXYbutton, SIGNAL(clicked()), this, SLOT(startIVandXYscan()));
  probercontrol->scanXYbutton->setText("Start XY+IV Scan");
  flagAbortScan = false;
  ivscan->startButton->setEnabled(true);

  std::chrono::time_point<std::chrono::system_clock> endTimeIVandXYscan = std::chrono::system_clock::now();
  std::time_t endTimeIVandXYscan2 = std::chrono::system_clock::to_time_t(endTimeIVandXYscan);
  std::cout << std::endl << "XY and IV scan finished at " << std::ctime(&endTimeIVandXYscan2); // << std::endl;
  std::cout << "Duration of whole scan [s]: " <<  std::chrono::duration_cast<std::chrono::seconds>(endTimeIVandXYscan - startTimeIVandXYscan).count();
  std::cout << ", i.e. [min]: " <<  std::chrono::duration_cast<std::chrono::minutes>(endTimeIVandXYscan - startTimeIVandXYscan).count() << std::endl;
}

void IVandXYscan::enableIVandXYscan(){
  if(probercontrol->checkBoxIVafterStep->isChecked()){
    disconnect(probercontrol->scanXYbutton, SIGNAL(clicked()), probercontrol, SLOT(scanXY()));
    connect(probercontrol->scanXYbutton, SIGNAL(clicked()), this, SLOT(startIVandXYscan()));
    probercontrol->scanXYbutton->setText("Start XY+IV Scan");
  }
  else if(!probercontrol->checkBoxIVafterStep->isChecked()){
    disconnect(probercontrol->scanXYbutton, SIGNAL(clicked()), this, SLOT(startIVandXYscan()));
    connect(probercontrol->scanXYbutton, SIGNAL(clicked()), probercontrol, SLOT(scanXY()));
    probercontrol->scanXYbutton->setText("Start XY Scan");
  }
 
}

void IVandXYscan::flagAbortScanToTrue(){
  std::cout << "ABORT scan clicked! End IV + XY scan and go back to default values." << std::endl;
  this->flagAbortScan = true;
  ivscan->flagAbortScanToTrue();
}



int main( int argc, char** argv )
{

  // start root and QT application
  QRootApplication app( argc, argv);
  app.setStyleSheet("QDockWidget { font-size: 14pt; }");

  // create main data viewer window
  IVandXYscan *Win = new IVandXYscan(0, Qt::Window);
  Win->show();

  //IVscan * ivscan = new IVscan(0, Qt::Window);
  //ivscan->show();

  // executing our application
  int ret = 0;
  std::stringstream msg;
  try{
    app.startTimer(); // needed to start TApplication event loop
    ret  = app.exec();
  } catch(std::exception& s){
    msg << "Std-lib exception \"";
    msg << s.what();
  } catch(...){
    msg << "Unknown exception \"";
  }
  if(msg.str()!="")
    std::cerr << msg.str() << "\" not caught during execution of main window." << std::endl;
  // cleaning up
  delete Win;

  return ret;
}
