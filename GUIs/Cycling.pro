TEMPLATE = app

CONFIG += qt
equals(QT_MAJOR_VERSION, 5) {
  QT+= widgets
}
unix {
  GCC_VERSION = $$system("g++ --version | grep g++")
  contains(GCC_VERSION, [5-9].[0-9].[0-9]) {
     CONFIG += c++14
  } else {
     contains(GCC_VERSION, 4.[8-9].[0-9]) {
       CONFIG += c++11
     } else {
       message( "unknown g++ version: ")
       message($$GCC_VERSION)
     }
  }
} else {
  CONFIG += c++11
}
CONFIG -= debug
CONFIG -= debug_and_release
CONFIG += release

INCLUDEPATH += . ../include ../PixGPIB ../PixRS232 ../simpati

FORMS += Cycling.ui

SOURCES += Cycling.cxx \
	   CyclingMain.cxx

HEADERS += Cycling.h

unix {
    DESTDIR =  .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
	QMAKE_LFLAGS += -lpthread -lboost_system -lgpib
        QMAKE_LFLAGS  +=  -L../simpati -l simpati -L../PixGPIB -lPixGPIB -L../PixRS232 -l PixRS232
	INCLUDEPATH += /usr/include/python2.7
	LIBS += -L /usr/local/lib/python2.7 -lpython2.7
        QMAKE_RPATHDIR += ../simpati ../PixGPIB ../PixRS232
}

win32 {
    DESTDIR =  ../bin
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
    QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
    QMAKE_LFLAGS_RELEASE = delayimp.lib simpati.lib
    QMAKE_LFLAGS_WINDOWS += /LIBPATH:../bin
}

