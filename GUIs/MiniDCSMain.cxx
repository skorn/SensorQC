#include "TempMon.h"
#include "DataLogging.h"
#include "SuppControl.h"
#include "QRootApplication.h"
#include <QMainWindow>
#include <QDockWidget>

#include <iostream>
#include <sstream>
#include <exception>

int main( int argc, char** argv )
{

  // start root and QT application
  QRootApplication app( argc, argv);

  // create main data viewer window
  QMainWindow *Win = new QMainWindow(0);
  Win->setWindowTitle("Supply and Environmental Control/Monitoring");

  QDockWidget* dockWidget_1 = new QDockWidget(Win);
  dockWidget_1->setObjectName(QStringLiteral("dockWidget_1"));
  dockWidget_1->setWindowTitle("Environment Monitors");
  QDockWidget* dockWidget_2 = new QDockWidget(Win);
  dockWidget_2->setObjectName(QStringLiteral("dockWidget_2"));
  dockWidget_2->setWindowTitle("Data Logging");
  QDockWidget* dockWidget_3 = new QDockWidget(Win);
  dockWidget_3->setObjectName(QStringLiteral("dockWidget_3"));
  dockWidget_3->setWindowTitle("Supply Control");

  TempMon *tm = new TempMon(Win, Qt::Window);
  dockWidget_1->setWidget(tm);
  DataLogging *dl = new DataLogging(Win, Qt::Window);
  dockWidget_2->setWidget(dl);
  SuppControl *sc = new SuppControl(Win, Qt::Window, false);
  dockWidget_3->setWidget(sc);
  Win->addDockWidget(Qt::RightDockWidgetArea,  dockWidget_1);
  Win->addDockWidget(Qt::RightDockWidgetArea,  dockWidget_2);
  Win->addDockWidget(Qt::LeftDockWidgetArea,   dockWidget_3);

  QWidget::connect(tm, SIGNAL(newTempVal(QMap<QString, double>)), dl, SLOT(newValues(QMap<QString, double>)) );
  QWidget::connect(tm, SIGNAL(obsTempDev(QList<QString>)), dl, SLOT(obsValues(QList<QString>)) );
  QWidget::connect(sc, SIGNAL(newSuppRdg(QMap<QString, double>)), dl, SLOT(newValues(QMap<QString, double>)) );
  QWidget::connect(sc, SIGNAL(obsSuppply(QList<QString>)), dl, SLOT(obsValues(QList<QString>)) );

  Win->show();

  // executing our application
  int ret = 0;
  std::stringstream msg;
  try{
    app.startTimer();
    ret  = app.exec();
  } catch(std::exception& s){
    msg << "Std-lib exception \"";
    msg << s.what();
  } catch(...){
    msg << "Unknown exception \"";
  }
  if(msg.str()!="")
    std::cerr << msg.str() << "\" not caught during execution of main window." << std::endl;
  // cleaning up
  delete Win;

  return ret;
}
