#include "Cycling.h"
#include <QTimer>
#include <QFileDialog>
#include <QDateTime>

#include "PixGPIBDevice.h"
#include "PixRs232Device.h"

#include <simpati.h>

#include <math.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <Python.h>

Cycling::Cycling(QWidget * parent, Qt::WindowFlags flags)
  : QWidget(parent, flags) {
  setupUi(this);
  m_timer = new QTimer(this);
  m_cycTimer = new QTimer(this);

  maxNumInstr = 2;

  m_setVal[0] = setVal_0;
  m_setVal[1] = setVal_1;
  m_setVal[2] = setVal_2;
  m_setVal[3] = setVal_3;
  m_setVal[4] = setVal_4;
  m_measVal[0] = measVal_0;
  m_measVal[1] = measVal_1;
  m_measVal[2] = measVal_2;
  m_measVal[3] = measVal_3;
  m_measVal[4] = measVal_4;
  m_measLabels[0] = measLabel_0;
  m_measLabels[1] = measLabel_1;
  m_measLabels[2] = measLabel_2;
  m_measLabels[3] = measLabel_3;
  m_measLabels[4] = measLabel_4;

   // fill vectors of instrument parameters
  activate_checkBox_.push_back(activate_checkBox_0); activate_checkBox_.push_back(activate_checkBox_1);
  itemLabel_.push_back(itemLabel_0); itemLabel_.push_back(itemLabel_1);
  gpibPAD_.push_back(gpibPAD_0); gpibPAD_.push_back(gpibPAD_1);
  instrumentChan_.push_back(instrumentChan_0); instrumentChan_.push_back(instrumentChan_1);
  measRes_.push_back(measRes_0); measRes_.push_back(measRes_1);
  gpibMessage_.push_back(gpibMessage_0); gpibMessage_.push_back(gpibMessage_1);
  measOhmLabel_.push_back(measOhmLabel_0); measOhmLabel_.push_back(measOhmLabel_1);
  chanLabel_.push_back(chanLabel_0); chanLabel_.push_back(chanLabel_1);
  instrumentLabel_.push_back(instrumentLabel_0); instrumentLabel_.push_back(instrumentLabel_1);
  gpibLabel_.push_back(gpibLabel_0); gpibLabel_.push_back(gpibLabel_1);

  for(int i=0; i< maxNumInstr; i++) {
    m_ConnInstr_.push_back(NULL); // initialise connected instruments with NULL
    isImeter_.push_back(false); // initialise Meter with false
  }

  connArduino();
  m_chamber = NULL;
  disconnDev();
  disconnDevAllGPIB();
  connect(startButton, SIGNAL(clicked()), this, SLOT(startCycling1()));
  connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDevAllGPIB()));
}

Cycling::~Cycling(){
  disconnDev();
}

void Cycling::connArduino(){
  // This function enables the serial connection with the arduino using the Connect_Arduino python script
  // The serial port for this connection is hard coded in Connect_Arduino.py and set to /dev/ttyACM0
  std::string Cycling_BASE_DIR = getenv("Cycling_BASE_DIR");
  std::string PYTHON_SERIAL_FILE_NAME = Cycling_BASE_DIR+"/GUIs/Connect_Arduino.py";
  Py_Initialize();
  // This solution is not good at the moment and should be replaced by a purely C++ based one.
  PyObject* PyFileObject = PyFile_FromString((char*)PYTHON_SERIAL_FILE_NAME.c_str(), "r");
  PyRun_SimpleFileEx(PyFile_AsFile(PyFileObject), (char*)PYTHON_SERIAL_FILE_NAME.c_str(), 1);

  return;
}

int Cycling::disconnArduino(){
  // To be implemented
  return 0;
}

void Cycling::connDev(){

  m_chamber = new simpati(ipAddress->text().toStdString(), portBox->value(), idBox->value());
  std::map<int, std::string> names = m_chamber->getNames();
  int k=0;
  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
    m_setVal[k]->show();
    m_measVal[k]->show();
    m_measLabels[k]->show();
    m_measLabels[k]->setText(it->second.c_str());
    k++;
  }

  m_timer->start(2000.);
  connect(m_timer, SIGNAL(timeout()), this, SLOT(readMeas()));

  startButton->setEnabled(true);

  connectButton->setText("Disconnect");
  disconnect(connectButton, SIGNAL(clicked()), this, SLOT(connDev()));
  connect(connectButton, SIGNAL(clicked()), this, SLOT(disconnDev()));
}
void Cycling::disconnDev(){
  m_timer->stop();
  disconnect(m_timer, SIGNAL(timeout()), this, SLOT(readMeas()));

  if(m_chamber!=NULL) m_chamber->setEnabled(false);
  delete m_chamber; m_chamber=NULL;

  for(uint i=0;i<nitems;i++){
    m_setVal[i]->hide();
    m_measVal[i]->hide();
    m_measLabels[i]->hide();
  }

  startButton->setEnabled(false);

  connectButton->setText("Connect");
  connect(connectButton, SIGNAL(clicked()), this, SLOT(connDev()));
  connect(activate_checkBox_0, SIGNAL(stateChanged(int)), this, SLOT(UpdateChainTab()));
  connect(activate_checkBox_1, SIGNAL(stateChanged(int)), this, SLOT(UpdateTempTab()));
  disconnect(connectButton, SIGNAL(clicked()), this, SLOT(disconnDev()));
}

void Cycling::UpdateChainTab() {
  bool status = activate_checkBox_0->isChecked();
  fileNameBaseLabel_0->setEnabled(status);
  fileNameBaseText_0->setEnabled(status);
  folderLabel_0->setEnabled(status);
  folderText_0->setEnabled(status);
  storage_label_0->setEnabled(status);
  chanLabel_0->setEnabled(status);
  gpibLabel_0->setEnabled(status);
  gpibPAD_0->setEnabled(status);
  instrumentChan_0->setEnabled(status);
  instrumentLabel_0->setEnabled(status);
  itemLabel_0->setEnabled(status);
  measRes_0->setEnabled(status);
  measOhmLabel_0->setEnabled(status);
  Chain_ID->setEnabled(status);
  ID_Progress->setEnabled(status);
  broken_label->setEnabled(status);
  broken_lcd->setEnabled(status);
}

void Cycling::UpdateTempTab() {
  bool status = activate_checkBox_1->isChecked();
  fileNameBaseLabel_1->setEnabled(status);
  fileNameBaseText_1->setEnabled(status);
  folderLabel_1->setEnabled(status);
  folderText_1->setEnabled(status);
  storage_label_1->setEnabled(status);
  chanLabel_1->setEnabled(status);
  gpibLabel_1->setEnabled(status);
  gpibPAD_1->setEnabled(status);
  instrumentChan_1->setEnabled(status);
  instrumentLabel_1->setEnabled(status);
  itemLabel_1->setEnabled(status);
  measRes_1->setEnabled(status);
  measOhmLabel_1->setEnabled(status);
}

void Cycling::connDevAllGPIB() {
  bool allDevicesGood = true;
  // connect all devices selected as source or meter
  for(int i=0; i<maxNumInstr; i++) {
    if (activate_checkBox_[i]->isChecked()) { 
      int returnValue = connDevGPIB(i);
      if(returnValue==-1) {
        allDevicesGood=false;
        break;
      }
    }
  }
  gpibConnectButton->setText("Disconnect Selected");
  disconnect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDevAllGPIB()));
  connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(disconnDevAllGPIB()));
  startButton->setEnabled(true);
  m_ChainthrRuns = true;
  m_TempthrRuns = true;
  m_Chainthread = std::thread(&Cycling::readChainsthread, this);
  m_Tempthread = std::thread(&Cycling::readTempthread, this);
  if(!idxConnInstr_.size()) {
    std::cout <<"No HV source or I meter connected. Try again!"<<std::endl;
    disconnDevAllGPIB();
  }
  else if(!allDevicesGood)  {
    std::cout << "Not all devices good. Aborting connection of all instruments!"<<std::endl;
    disconnDevAllGPIB();
  }
  else std::cout <<"Connected "<<idxConnInstr_.size()<<" instruments."<<std::endl;
}

int Cycling::connDevGPIB(int instrIdx) {

  isImeter_[instrIdx] = true;
  bool forceMeter = false;

  //check if instrument has been already connected
  if(m_ConnInstr_[instrIdx]!=NULL){
    std::cout << "Instrument with index "<<instrIdx<<" already connected (NOT NULL). Disconnect first before re-connecting!!"<<std::endl;
    return 0;
  }
  for (uint i=0; i<idxConnInstr_.size(); i++){
    if(instrIdx==idxConnInstr_[i]){
    std::cout << "Instrument with index "<<instrIdx<<" already connected. Disconnect first before re-connecting!!"<<std::endl;
    return 0;
    }
    if(gpibPAD_[instrIdx]==gpibPAD_[idxConnInstr_[i]]){
    std::cout << "Istrument with GPIB PAD "<<gpibPAD_[instrIdx]<<" already connected. Disconnect first before re-connecting!!"<<std::endl;
    return 0;
    }
  }

  // Manually set the current limit to 10 micro amps
  double currentLimit = 10*1E-6;

  idxConnInstr_.push_back(instrIdx);

  if(gpibLabel_[instrIdx]->text().contains("GPIB")){

    PixGPIBDevice* thisInstr = new PixGPIBDevice(0, gpibPAD_[instrIdx]->value(), 1, forceMeter);

    if(thisInstr->getDeviceFunction()==SUPPLY_HV || thisInstr->getDeviceFunction()==METER){ // check if instrument is the proper one
      m_ConnInstr_[instrIdx] = (void*)thisInstr;
      gpibMessage_[instrIdx]->setText("Conn. device is "+QString(thisInstr->getDescription())+
				      " with "+
				      QString::number(thisInstr->getDeviceNumberChannels())+
				      " ch.");
      instrumentChan_[instrIdx]->setValue(0);
      instrumentChan_[instrIdx]->setMaximum(thisInstr->getDeviceNumberChannels()-1);
      instrumentChan_[instrIdx]->setMinimum(0);

      if(thisInstr->getDeviceFunction()==SUPPLY_HV){
	thisInstr->setVoltage(0, 0.0);
	thisInstr->setCurrentLimit(0, currentLimit);
      }
      thisInstr->setState(PixGPIBDevice::PGD_ON);
      std::this_thread::sleep_for (std::chrono::milliseconds(100));

      gpibPAD_[instrIdx]->setEnabled(false);
      measRes_[instrIdx]->display(0.);

      std::cout<<"Connected instrument with index " << instrIdx <<": "<<thisInstr->getDescription();
      if      (thisInstr->getDeviceFunction()==SUPPLY_HV) std::cout << "  Function: HV source."  << std::endl;
      else if (thisInstr->getDeviceFunction()==METER)     std::cout << "  Function: AMPERE METER."  << std::endl;
    } else {
      delete thisInstr;
      m_ConnInstr_[instrIdx] = NULL;
    }
  } else { // asume it's a RS232 device
    PixRs232Device* thisInstr = new PixRs232Device((PixRs232Device::Portids)(gpibPAD_[instrIdx]->value()-1+PixRs232Device::COM1));
    if(thisInstr->getDeviceFunction()==LCRMETER){ // check if instrument is the proper one
      m_ConnInstr_[instrIdx] = (void*)thisInstr;
      gpibMessage_[instrIdx]->setText("Conn. device is "+QString(thisInstr->getDescription())+
				      " with "+
				      QString::number(thisInstr->getDeviceNumberChannels())+
				      " ch.");
      instrumentChan_[instrIdx]->setValue(0);
      instrumentChan_[instrIdx]->setMaximum(thisInstr->getDeviceNumberChannels()-1);
      instrumentChan_[instrIdx]->setMinimum(0);

      gpibPAD_[instrIdx]->setEnabled(false);
      measRes_[instrIdx]->display(0.);

      std::cout<<"Connected instrument with index " << instrIdx <<": "<<thisInstr->getDescription()<<std::endl;
    } else {
      delete thisInstr;
      m_ConnInstr_[instrIdx] = NULL;
    }
  }

  if(m_ConnInstr_[instrIdx] == NULL) {
    gpibMessage_[instrIdx]->setText("ERROR connecting");
    std::cout<<"ERROR connecting instrument with index " << instrIdx <<std::endl;
    instrumentLabel_[instrIdx]->setEnabled(false);
    itemLabel_[instrIdx]->setEnabled(false);
    gpibLabel_[instrIdx]->setEnabled(false);
    gpibPAD_[instrIdx]->setEnabled(false);
    measRes_[instrIdx]->setEnabled(false);
    measOhmLabel_[instrIdx]->setEnabled(false);
    instrumentChan_[instrIdx]->setEnabled(false);
    chanLabel_[instrIdx]->setEnabled(false);
    return -1;
  } else
    return 1;

}

void Cycling::disconnDevGPIB(int instrIdx) {

  // enable again all devices

  instrumentChan_[instrIdx]->setValue(0);
  instrumentChan_[instrIdx]->setMaximum(99);
  instrumentChan_[instrIdx]->setMinimum(0);

  // return if device already not connected
  if(m_ConnInstr_[instrIdx]==NULL) return;

  if(gpibLabel_[instrIdx]->text().contains("GPIB")){
    PixGPIBDevice* dev = (PixGPIBDevice*)m_ConnInstr_[instrIdx];
    dev->setVoltage(0, 0.0);
    dev->setState(PixGPIBDevice::PGD_OFF);
    std::this_thread::sleep_for (std::chrono::milliseconds(100));
    delete dev;
  } else {
    PixRs232Device* dev = (PixRs232Device*)m_ConnInstr_[instrIdx];
    delete dev;
  }
  m_ConnInstr_[instrIdx] = NULL;
  gpibMessage_[instrIdx]->setText("");
}

void Cycling::readChainsthread() {
  QString ChainfileNameQ = folderText_0->text();
  ChainfileNameQ += fileNameBaseText_0->text();
  std::string fileName;
  fileName = ChainfileNameQ.toStdString();
  // ofstream to save chain resistance data
  std::ofstream ChainoutTxtFile;
  ChainoutTxtFile.open((fileName+".dat").c_str(), std::ofstream::out | std::ofstream::trunc);
  while(m_ChainthrRuns){
    readChains(fileName);
    QApplication::processEvents(); // update GUI
    std::this_thread::sleep_for (std::chrono::milliseconds(500));
  }
}

void Cycling::readTempthread() {
  QString TempfileNameQ = folderText_1->text();
  TempfileNameQ += fileNameBaseText_1->text();
  std::string fileName;
  fileName = TempfileNameQ.toStdString();
  // ofstream to save chain resistance data
  std::ofstream TempoutTxtFile;
  TempoutTxtFile.open((fileName+".dat").c_str(), std::ofstream::out | std::ofstream::trunc);
  while(m_TempthrRuns){
    readTemp(fileName);
    QApplication::processEvents(); // update GUI
    std::this_thread::sleep_for (std::chrono::milliseconds(500));
  }
}

void Cycling::readChains(std::string fileName) {

  // ofstream to talk to arduino
  std::ofstream arduino;
  // ofstream to save chain resistance data
  std::ofstream ChainoutTxtFile;
  if(isImeter_[0]){
    if(gpibLabel_[0]->text().contains("GPIB")){
      PixGPIBDevice* dev = (PixGPIBDevice*) m_ConnInstr_[0];
        broken_counter = 0;
	for(int j=1; j<=336;j++) {
          std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
	  std::time_t scan_time = std::chrono::system_clock::to_time_t(start);
	  std::tm* timeinfo;
	  char timeChar [80];
	  std::time(&scan_time);
	  timeinfo = std::localtime(&scan_time);
	  std::strftime(timeChar,80,"%Y-%m-%d %H:%M:%S",timeinfo); //custom date format
	  arduino.open("/dev/ttyACM0");
	  arduino << LookUp(j);
          dev->measureResistances(1., true, 0);
          Chain_R = dev->getResistance(0);
	  if(Chain_R > 1e3) broken_counter++;
	  measRes_[0]->display(Chain_R);
          ID_Progress->display(j);
	  arduino.close();
	  std::this_thread::sleep_for(std::chrono::milliseconds(10));
	  ChainoutTxtFile.open((fileName+".dat").c_str(), std::ofstream::out |std::ofstream::app);
	  if (j==336)ChainoutTxtFile <<  timeChar  << "\t" << Chain_R;
	  else ChainoutTxtFile <<  timeChar  << "\t" << Chain_R <<"\t";
	  ChainoutTxtFile.close();
      }
      /*
      else {
	PixRs232Device* dev = (PixRs232Device*) m_ConnInstr_[idxConnInstr_[i]];
	double freq = capFreqBox->currentText().toDouble()*1.e3;
	double veff = boxCVeff->value();
	double circtype;
	QString circtypestring = capCircuitBox->currentText();
	if(circtypestring == "Par") circtype=1;
	else if(circtypestring == "Ser") circtype=0;
	else circtype=2;
	measRes_[idxConnInstr_[i]]->display(dev->getCapacity(instrumentChan_[idxConnInstr_[i]]->value(), freq, veff, circtype, true)*1.e12);
      }
      */
      broken_lcd->display(broken_counter);
      ChainoutTxtFile.open((fileName+".dat").c_str(), std::ofstream::out | std::ofstream::app);
      ChainoutTxtFile << "\n";
      ChainoutTxtFile.close();
    }
  }
}

void Cycling::readTemp(std::string fileName) {
  // ofstream to save chain resistance data
  std::ofstream TempoutTxtFile;
  if(isImeter_[1]){
    if(gpibLabel_[1]->text().contains("GPIB")){
      PixGPIBDevice* dev = (PixGPIBDevice*) m_ConnInstr_[1];
      std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
      std::time_t scan_time = std::chrono::system_clock::to_time_t(start);
      std::tm* timeinfo;
      char timeChar [80];
      std::time(&scan_time);
      timeinfo = std::localtime(&scan_time);
      std::strftime(timeChar,80,"%Y-%m-%d %H:%M:%S",timeinfo); //custom date format
      dev->measureResistances(1., true, 0);
      Temp_R = dev->getResistance(instrumentChan_[1]->value());
      measRes_[1]->display(Temp_R);
      /*
      else {
	PixRs232Device* dev = (PixRs232Device*) m_ConnInstr_[idxConnInstr_[i]];
	double freq = capFreqBox->currentText().toDouble()*1.e3;
	double veff = boxCVeff->value();
	double circtype;
	QString circtypestring = capCircuitBox->currentText();
	if(circtypestring == "Par") circtype=1;
	else if(circtypestring == "Ser") circtype=0;
	else circtype=2;
	measRes_[idxConnInstr_[i]]->display(dev->getCapacity(instrumentChan_[idxConnInstr_[i]]->value(), freq, veff, circtype, true)*1.e12);
      }
      */
      TempoutTxtFile.open((fileName+".dat").c_str(), std::ofstream::out |std::ofstream::app);
      TempoutTxtFile <<  timeChar  << "\t" << Temp_R <<"\n";
      TempoutTxtFile.close();
    }
  }
}

void Cycling::readMeas(){
  if(m_chamber==0) return;
  std::map<int, std::string> names = m_chamber->getNames();
  int k=0;
  QString SimpatiDataoutfileNameQ = chamberstorage->text();
  SimpatiDataoutfileNameQ += ChamberFileNameBase->text();
  std::string fileName;
  fileName = SimpatiDataoutfileNameQ.toStdString();
  std::ofstream SimpatiDataoutTxtFile;
  SimpatiDataoutTxtFile.open((fileName+".dat").c_str(), std::ofstream::out |std::ofstream::app);
  std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
  std::time_t scan_time = std::chrono::system_clock::to_time_t(start);
  std::tm* timeinfo;
  char timeChar [80];
  std::time(&scan_time);
  timeinfo = std::localtime(&scan_time);
  std::strftime(timeChar,80,"%Y-%m-%d %H:%M:%S",timeinfo); //custom date format
  SimpatiDataoutTxtFile <<  timeChar;
  std::cout << timeChar;
  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
    std::pair<double, double> vals = m_chamber->getVals()[it->first];
    m_setVal[k]->display(vals.first);
    m_measVal[k]->display(vals.second);
    SimpatiDataoutTxtFile << "\t" <<  vals.second;
    std::cout << "\t" <<  vals.second;
    k++;
  }
  std::cout << "\n";
  SimpatiDataoutTxtFile << "\n";
  SimpatiDataoutTxtFile.close();
}

void Cycling::disconnDevAllGPIB() {
  // disconnect all devices selected as source or meter
  m_ChainthrRuns = false;
  m_TempthrRuns = false;
  if(m_Chainthread.joinable()) m_Chainthread.join();
  if(m_Tempthread.joinable()) m_Tempthread.join();
  for(int i=0; i<maxNumInstr; i++) disconnDevGPIB(i);
  gpibConnectButton->setText("Connect Selected");
  disconnect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(disconnDevAllGPIB()));
  connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDevAllGPIB()));
  idxConnInstr_.clear();
  isImeter_.clear();
  std::cout<<"Disonnected all instruments." <<std::endl;
}

void Cycling::startCycling1(){
  startButton->setText("Abort");
  startButton->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 0, 0);"));
  disconnect(startButton, SIGNAL(clicked()), this, SLOT(startCycling1()));
  connect(startButton, SIGNAL(clicked()), this, SLOT(stopCycling()));

  if(prepareBox->isChecked()){
    m_chamber->setTwarm(preWarmBox->value());
    m_chamber->setTcold(preColdBox->value());
    m_chamber->setCage(preTopButton->isChecked(), true);
    m_chamber->setEnabled(true);
    m_cycTimer->start(2000.);
    connect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling1()));
  } else
    startCycling2();
}
void Cycling::startCycling2(){
  m_chamber->startCyclingPrg(cycleProgBox->value());
  m_cycTimer->start(2000.);
  connect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling2()));
}
void Cycling::startCycling3(){
  if(finishBox->isChecked()){
    m_chamber->setTwarm(postWarmBox->value());
    m_chamber->setTcold(postColdBox->value());
    m_chamber->setEnabled(true);
    m_cycTimer->start(2000.);
    connect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling3()));
  } else{
    endCycling();
    m_chamber->setEnabled(false);
  }
}
void Cycling::monitorCycling1(){
  double Twarm=-9999., Tcold=-9999.;
  std::map<int, std::string> names = m_chamber->getNames();
  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
    std::pair<double, double> vals = m_chamber->getVals()[it->first];
    if(names[it->first].substr(0,10)=="Warmkammer") Twarm = vals.second;
    if(names[it->first].substr(0,10)=="Kaltkammer") Tcold = vals.second;
  }
  if(fabs(Twarm-preWarmBox->value())<tmatchPre->value() &&
     fabs(Tcold-preColdBox->value())<tmatchPre->value()){
    m_cycTimer->stop();
    disconnect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling1()));
    startCycling2();
  }
}
void Cycling::monitorCycling2(){
  if(m_chamber->getChamberStatus()==0){
    m_cycTimer->stop();
    disconnect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling2()));
    startCycling3();
  }
}
void Cycling::monitorCycling3(){
  double Tcold=-9999.;//Twarm=-9999.
  std::map<int, std::string> names = m_chamber->getNames();
  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
    std::pair<double, double> vals = m_chamber->getVals()[it->first];
    //    if(names[it->first].substr(0,10)=="Warmkammer") Twarm = vals.second;
    if(names[it->first].substr(0,10)=="Kaltkammer") Tcold = vals.second;
  }
  if(fabs(Tcold-postColdBox->value())<tmatchPost->value()){
    m_cycTimer->stop();
    disconnect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling3()));
    m_chamber->setCage(postTopButton->isChecked(), true);
    if(finishMeasBox->isChecked() && finishMeasBox->isEnabled()){
      measDoneButton->setEnabled(true);
      connect(measDoneButton, SIGNAL(clicked()), this, SLOT(endCycling()));
      emit readyForMeasurement();
    } else
      endCycling();
  }
}
void Cycling::endCycling(){
  measDoneButton->setEnabled(false);
  m_chamber->setEnabled(false);
  startButton->setText("Start");
  startButton->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 255, 0);"));
  disconnect(startButton, SIGNAL(clicked()), this, SLOT(stopCycling()));
  connect(startButton, SIGNAL(clicked()), this, SLOT(startCycling1()));
  disconnDevAllGPIB();
}
void Cycling::stopCycling(){
  m_cycTimer->stop();
  disconnect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling1()));
  disconnect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling2()));
  disconnect(m_cycTimer, SIGNAL(timeout()), this, SLOT(monitorCycling3()));
  m_chamber->stopCycling();
  endCycling();
}

std::string Cycling::LookUp(int i){
switch ( i )
  {
  case 1:
    return "28 28 42 45;";
    
  case 2:
    return "28 27 42 45;";

  case 3:
    return "27 27 42 45;";
    
  case 4:
    return "27 26 42 45;";
    
  case 5:
    return "26 26 42 45;";
    
  case 6:
    return "26 25 42 45;";
    
  case 7:
    return "25 25 42 45;";
    
  case 8:
    return "25 24 42 45;";
    
  case 9:
    return "24 24 42 45;";
    
  case 10:
    return "24 23 42 45;";
    
  case 11:
    return "23 23 42 45;";
    
  case 12:
    return "23 22 42 45;";
    
  case 13:
    return "22 22 42 45;";
    
  case 14:
    return "22 21 42 45;";
    
  case 15:
    return "21 21 42 45;";
    
  case 16:
    return "21 20 42 45;";
    
  case 17:
    return "20 20 42 45;";
    
  case 18:
    return "20 0 42 33;";
    
  case 19:
    return "0 0 30 33;";
    
  case 20:
    return "0 1 30 33;";
    
  case 21:
    return "1 1 30 33;";
    
  case 22:
    return "1 2 30 33;";
    
  case 23:
    return "2 2 30 33;";
    
  case 24:
    return "2 3 30 33;";
    
  case 25:
    return "3 3 30 33;";
    
  case 26:
    return "3 4 30 33;";
    
  case 27:
    return "4 4 30 33;";
    
  case 28:
    return "4 5 30 33;";
    
  case 29:
    return "5 5 30 33;";
    
  case 30:
    return "5 6 30 33;";
    
  case 31:
    return "6 6 30 33;";
    
  case 32:
    return "6 7 30 33;";
    
  case 33:
    return "7 7 30 33;";
    
  case 34:
    return "7 8 30 33;";
    
  case 35:
    return "8 8 30 33;";

  case 36:
    return "8 9 30 33;";

  case 37:
    return "9 9 30 33;";

  case 38:
    return "9 10 30 33;";

  case 39:
    return "10 10 30 33;";

  case 40:
    return "10 11 30 33;";

  case 41:
    return "11 11 30 33;";

  case 42:
    return "11 12 30 33;";

  case 43:
    return "12 12 30 33;";

  case 44:
    return "12 13 30 33;";

  case 45:
    return "13 13 30 33;";

  case 46:
    return "13 14 30 33;";

  case 47:
    return "14 14 30 33;";

  case 48:
    return "14 15 30 33;";

  case 49:
    return "15 15 30 33;";

  case 50:
    return "15 29 30 45;";

  case 51:
    return "29 29 42 45;";

  case 52:
    return "29 30 42 45;";

  case 53:
    return "30 30 42 45;";

  case 54:
    return "30 31 42 45;";

  case 55:
    return "31 31 42 45;";

  case 56:
    return "31 15 42 45;";

  case 57:
    return "15 15 42 45;";

  case 58:
    return "15 14 42 45;";

  case 59:
    return "14 14 42 45;";

  case 60:
    return "14 13 42 45;";

  case 61:
    return "13 13 42 45;";

  case 62:
    return "13 12 42 45;";

  case 63:
    return "12 12 42 45;";

  case 64:
    return "12 11 42 45;";

  case 65:
    return "11 11 42 45;";

  case 66:
    return "11 10 42 45;";

  case 67:
    return "10 10 42 45;";

  case 68:
    return "10 16 42 33;";

  case 69:
    return "16 16 30 33;";

  case 70:
    return "16 17 30 33;";

  case 71:
    return "17 17 30 33;";

  case 72:
    return "17 18 30 33;";

  case 73:
    return "18 18 30 33;";

  case 74:
    return "18 19 30 33;";

  case 75:
    return "19 19 30 33;";

  case 76:
    return "19 20 30 33;";

  case 77:
    return "20 20 30 33;";

  case 78:
    return "20 21 30 33;";

  case 79:
    return "21 21 30 33;";

  case 80:
    return "21 22 30 33;";

  case 81:
    return "22 22 30 33;";

  case 82:
    return "22 23 30 33;";

  case 83:
    return "23 23 30 33;";

  case 84:
    return "23 24 30 33;";

  case 85:
    return "24 24 30 33;";

  case 86:
    return "24 25 30 33;";

  case 87:
    return "25 25 30 33;";

  case 88:
    return "25 26 30 33;";

  case 89:
    return "26 26 30 33;";

  case 90:
    return "26 27 30 33;";

  case 91:
    return "27 27 30 33;";

  case 92:
    return "27 28 30 33;";

  case 93:
    return "28 28 30 33;";

  case 94:
    return "28 29 30 33;";

  case 95:
    return "29 29 30 33;";

  case 96:
    return "29 30 30 33;";

  case 97:
    return "30 30 30 33;";

  case 98:
    return "30 31 30 33;";

  case 99:
    return "31 31 30 33;";

  case 100:
    return "31 5 30 49;";

  case 101:
    return "5 5 46 49;";

  case 102:
    return "5 6 46 49;";

  case 103:
    return "6 6 46 49;";

  case 104:
    return "6 7 46 49;";

  case 105:
    return "7 7 46 49;";

  case 106:
    return "7 8 46 49;";

  case 107:
    return "8 8 46 49;";

  case 108:
    return "8 9 46 49;";

  case 109:
    return "9 9 46 49;";

  case 110:
    return "9 10 46 49;";

  case 111:
    return "10 10 46 49;";

  case 112:
    return "10 11 46 49;";

  case 113:
    return "11 11 46 49;";

  case 114:
    return "11 12 46 49;";

  case 115:
    return "12 12 46 49;";

  case 116:
    return "12 13 46 49;";

  case 117:
    return "13 13 46 49;";

  case 118:
    return "13 0 46 37;";

  case 119:
    return "0 0 34 37;";

  case 120:
    return "0 1 34 37;";

  case 121:
    return "1 1 34 37;";

  case 122:
    return "1 2 34 37;";

  case 123:
    return "2 2 34 37;";

  case 124:
    return "2 3 34 37;";

  case 125:
    return "3 3 34 37;";

  case 126:
    return "3 4 34 37;";

  case 127:
    return "4 4 34 37;";

  case 128:
    return "4 5 34 37;";

  case 129:
    return "5 5 34 37;";

  case 130:
    return "5 6 34 37;";

  case 131:
    return "6 6 34 37;";

  case 132:
    return "6 7 34 37;";

  case 133:
    return "7 7 34 37;";

  case 134:
    return "7 8 34 37;";

  case 135:
    return "8 8 34 37;";

  case 136:
    return "8 9 34 37;";

  case 137:
    return "9 9 34 37;";

  case 138:
    return "9 10 34 37;";

  case 139:
    return "10 10 34 37;";

  case 140:
    return "10 11 34 37;";

  case 141:
    return "11 11 34 37;";

  case 142:
    return "11 12 34 37;";

  case 143:
    return "12 12 34 37;";

  case 144:
    return "12 13 34 37;";

  case 145:
    return "13 13 34 37;";

  case 146:
    return "13 14 34 37;";

  case 147:
    return "14 14 34 37;";

  case 148:
    return "14 15 34 37;";

  case 149:
    return "15 15 34 37;";

  case 150:
    return "15 4 34 49;";

  case 151:
    return "4 4 46 49;";

  case 152:
    return "4 3 46 49;";

  case 153:
    return "3 3 46 49;";

  case 154:
    return "3 2 46 49;";

  case 155:
    return "2 2 46 49;";

  case 156:
    return "2 1 46 49;";

  case 157:
    return "1 1 46 49;";

  case 158:
    return "1 0 46 49;";

  case 159:
    return "0 0 46 49;";

  case 160:
    return "0 16 46 45;";

  case 161:
    return "16 16 42 45;";

  case 162:
    return "16 17 42 45;";

  case 163:
    return "17 17 42 45;";

  case 164:
    return "17 18 42 45;";

  case 165:
    return "18 18 42 45;";

  case 166:
    return "18 19 42 45;";

  case 167:
    return "19 19 42 45;";

  case 168:
    return "19 16 42 37;";

  case 169:
    return "16 16 34 37;";

  case 170:
    return "16 17 34 37;";

  case 171:
    return "17 17 34 37;";

  case 172:
    return "17 18 34 37;";

  case 173:
    return "18 18 34 37;";

  case 174:
    return "18 19 34 37;";

  case 175:
    return "19 19 34 37;";

  case 176:
    return "19 20 34 37;";

  case 177:
    return "20 20 34 37;";

  case 178:
    return "20 21 34 37;";

  case 179:
    return "21 21 34 37;";

  case 180:
    return "21 22 34 37;";

  case 181:
    return "22 22 34 37;";

  case 182:
    return "22 23 34 37;";

  case 183:
    return "23 23 34 37;";

  case 184:
    return "23 24 34 37;";

  case 185:
    return "24 24 34 37;";

  case 186:
    return "24 25 34 37;";

  case 187:
    return "25 25 34 37;";

  case 188:
    return "25 26 34 37;";

  case 189:
    return "26 26 34 37;";

  case 190:
    return "26 27 34 37;";

  case 191:
    return "27 27 34 37;";

  case 192:
    return "27 28 34 37;";

  case 193:
    return "28 28 34 37;";

  case 194:
    return "28 29 34 37;";

  case 195:
    return "29 29 34 37;";

  case 196:
    return "29 30 34 37;";

  case 197:
    return "30 30 34 37;";

  case 198:
    return "30 31 34 37;";

  case 199:
    return "31 31 34 37;";

  case 200:
    return "31 24 34 49;";

  case 201:
    return "24 24 46 49;";

  case 202:
    return "24 23 46 49;";

  case 203:
    return "23 23 46 49;";

  case 204:
    return "23 22 46 49;";

  case 205:
    return "22 22 46 49;";

  case 206:
    return "22 21 46 49;";

  case 207:
    return "21 21 46 49;";

  case 208:
    return "21 20 46 49;";

  case 209:
    return "20 20 46 49;";

  case 210:
    return "20 19 46 49;";

  case 211:
    return "19 19 46 49;";

  case 212:
    return "19 18 46 49;";

  case 213:
    return "18 18 46 49;";

  case 214:
    return "18 17 46 49;";

  case 215:
    return "17 17 46 49;";

  case 216:
    return "17 16 46 49;";

  case 217:
    return "16 16 46 49;";

  case 218:
    return "16 0 46 41;";

  case 219:
    return "0 0 38 41;";

  case 220:
    return "0 1 38 41;";

  case 221:
    return "1 1 38 41;";

  case 222:
    return "1 2 38 41;";

  case 223:
    return "2 2 38 41;";

  case 224:
    return "2 3 38 41;";

  case 225:
    return "3 3 38 41;";

  case 226:
    return "3 4 38 41;";

  case 227:
    return "4 4 38 41;";

  case 228:
    return "4 5 38 41;";

  case 229:
    return "5 5 38 41;";

  case 230:
    return "5 6 38 41;";

  case 231:
    return "6 6 38 41;";

  case 232:
    return "6 7 38 41;";

  case 233:
    return "7 7 38 41;";

  case 234:
    return "7 8 38 41;";

  case 235:
    return "8 8 38 41;";

  case 236:
    return "8 9 38 41;";

  case 237:
    return "9 9 38 41;";

  case 238:
    return "9 10 38 41;";

  case 239:
    return "10 10 38 41;";

  case 240:
    return "10 11 38 41;";

  case 241:
    return "11 11 38 41;";

  case 242:
    return "11 12 38 41;";

  case 243:
    return "12 12 38 41;";

  case 244:
    return "12 13 38 41;";

  case 245:
    return "13 13 38 41;";

  case 246:
    return "13 14 38 41;";

  case 247:
    return "14 14 38 41;";

  case 248:
    return "14 15 38 41;";

  case 249:
    return "15 15 38 41;";

  case 250:
    return "15 25 38 49;";

  case 251:
    return "25 25 46 49;";

  case 252:
    return "25 26 46 49;";

  case 253:
    return "26 26 46 49;";

  case 254:
    return "26 27 46 49;";

  case 255:
    return "27 27 46 49;";

  case 256:
    return "27 28 46 49;";

  case 257:
    return "28 28 46 49;";

  case 258:
    return "28 29 46 49;";

  case 259:
    return "29 29 46 49;";

  case 260:
    return "29 30 46 49;";

  case 261:
    return "30 30 46 49;";

  case 262:
    return "30 31 46 49;";

  case 263:
    return "31 31 46 49;";

  case 264:
    return "31 15 46 49;";

  case 265:
    return "15 15 46 49;";

  case 266:
    return "15 14 46 49;";

  case 267:
    return "14 14 46 49;";

  case 268:
    return "14 16 46 41;";

  case 269:
    return "16 16 38 41;";

  case 270:
    return "16 17 38 41;";

  case 271:
    return "17 17 38 41;";

  case 272:
    return "17 18 38 41;";

  case 273:
    return "18 18 38 41;";

  case 274:
    return "18 19 38 41;";

  case 275:
    return "19 19 38 41;";

  case 276:
    return "19 20 38 41;";

  case 277:
    return "20 20 38 41;";

  case 278:
    return "20 21 38 41;";

  case 279:
    return "21 21 38 41;";

  case 280:
    return "21 22 38 41;";

  case 281:
    return "22 22 38 41;";

  case 282:
    return "22 23 38 41;";

  case 283:
    return "23 23 38 41;";

  case 284:
    return "23 24 38 41;";

  case 285:
    return "24 24 38 41;";

  case 286:
    return "24 25 38 41;";

  case 287:
    return "25 25 38 41;";

  case 288:
    return "25 26 38 41;";

  case 289:
    return "26 26 38 41;";

  case 290:
    return "26 27 38 41;";

  case 291:
    return "27 27 38 41;";

  case 292:
    return "27 28 38 41;";

  case 293:
    return "28 28 38 41;";

  case 294:
    return "28 29 38 41;";

  case 295:
    return "29 29 38 41;";

  case 296:
    return "29 30 38 41;";

  case 297:
    return "30 30 38 41;";

  case 298:
    return "30 31 38 41;";

  case 299:
    return "31 31 38 41;";

  case 300:
    return "31 0 38 45;";

  case 301:
    return "0 0 42 45;";

  case 302:
    return "0 1 42 45;";

  case 303:
    return "1 1 42 45;";

  case 304:
    return "1 2 42 45;";

  case 305:
    return "2 2 42 45;";

  case 306:
    return "2 3 42 45;";

  case 307:
    return "3 3 42 45;";

  case 308:
    return "3 4 42 45;";

  case 309:
    return "4 4 42 45;";

  case 310:
    return "4 5 42 45;";

  case 311:
    return "5 5 42 45;";

  case 312:
    return "5 6 42 45;";

  case 313:
    return "6 6 42 45;";

  case 314:
    return "6 7 42 45;";

  case 315:
    return "7 7 42 45;";

  case 316:
    return "7 8 42 45;";

  case 317:
    return "8 8 42 45;";

  case 318:
    return "8 9 42 45;";

  case 319:
    return "11 9 26 45;";

  case 320:
    return "11 11 26 29;";

  case 321:
    return "10 11 26 29;";

  case 322:
    return "10 10 26 29;";

  case 323:
    return "9 10 26 29;";

  case 324:
    return "9 9 26 29;";

  case 325:
    return "8 9 26 29;";

  case 326:
    return "8 8 26 29;";

  case 327:
    return "7 8 26 29;";

  case 328:
    return "7 7 26 29;";

  case 329:
    return "6 7 26 29;";

  case 330:
    return "6 6 26 29;";

  case 331:
    return "5 6 26 29;";

  case 332:
    return "5 5 26 29;";

  case 333:
    return "4 5 26 29;";

  case 334:
    return "4 4 26 29;";

  case 335:
    return "3 4 26 29;";

  case 336:
    return "3 3 26 29;";

  case 337:
    return "2 3 26 29;";

  default:
    return "";
  }
}
