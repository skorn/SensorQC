#ifndef SUPPCONTROL_H
#define SUPPCONTROL_H

#include <QWidget>
#include "ui_SuppControl.h"
#include <vector>

class SuppControlBase;

class SuppControl : public QWidget, public Ui::SuppControl {

    Q_OBJECT

 public:
  SuppControl(QWidget * parent = 0, Qt::WindowFlags flags = Qt::Window, bool isStandalone=true);
  ~SuppControl();

 public slots:
  void noDevChanged(int);
  void connectAllDev();
  void disconnectAllDev();

 signals:
  void newSuppply(QList<QString>);
  void obsSuppply(QList<QString>);
  void newSuppRdg(QMap<QString, double>);

 private:
  std::vector<SuppControlBase*> m_lvbases;
  bool m_isStandalone;

};
#endif // SUPPCONTROL_H
