#ifndef PROBERCONTROL_H
#define PROBERCONTROL_H

#include "ui_ProberControl.h"
#include <QWidget>
#include <thread>

namespace Suess{
  class GenericProber;
}

class ProberControl : public QWidget, public Ui::ProberControl {

    Q_OBJECT

 public:
    ProberControl(QWidget * parent = 0, Qt::WindowFlags flags = Qt::Window, bool showIV=false);
    ~ProberControl();
    void connectTimerToReadProberPosition(bool connectOrNot);
    Suess::GenericProber *m_prober;

 public slots:
   void connectProber();
   void disconnectProber();
   void readDev();
   void readDevThread();
   void transfer();
   void moveXY();
   void moveZ();
   void setT();
   void scanXY();
   void proberTypeSel();
   void sendCommand();

 signals:
   void runIV();

 private:
//   Suess::GenericProber *m_prober;
   // read-out thread
   std::thread m_thread;
   bool m_thrRuns;


};


#endif // PROBERCONTROL_H
